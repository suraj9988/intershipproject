import React, { Component } from "react";
import { Button, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            enterValue: ""
        }

    }

    onChangeText = (text) => {
        this.setState({ enterValue: text })
    }

    onButtonClicked = () => {
        console.log("on button click")
    }

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'flex-start'
            }}>
                {/* <Text style={styleSheet.textStyle}>
                    Hi My aplication
                </Text>

                <Text style={[styleSheet.textStyle, { alignSelf: 'center' }]}>
                    {this.state.enterValue}
                </Text>

                <TextInput
                    value={this.state.enterValue}
                    onChangeText={this.onChangeText}
                    placeholder="Enter the value"
                />

                <Button
                    onPress={this.onButtonClicked}
                    title="Sign In"
                    color="#841584"
                />

                <TouchableOpacity
                    style={styleSheet.buttonStyle}
                    onPress={this.onButtonClicked}
                >
                    <Text style={[styleSheet.textStyle, { color: 'white', fontWeight: 'normal' }]}>
                        Sign In
                    </Text>

                </TouchableOpacity> */}

                <View style={{ width: 100, height: 100, backgroundColor: 'green' }} />

                <View style={{ width: 100, height: 100, backgroundColor: 'black' }} />

            </View>
        )
    }
}

const styleSheet = StyleSheet.create({
    textStyle: {
        color: 'red',
        fontSize: 16,
        fontWeight: 'bold',
        alignSelf: 'flex-end'
    },

    buttonStyle: {

        margin: 10,
        padding: 10,
        backgroundColor: 'green',
        alignSelf: 'center',
        borderRadius: 4,
        borderColor: 'black',
        borderWidth: 1,
        elevation: 4,
        shadowOffset: { width: 10, height: 20 },
        shadowOpacity: 0.4,
        shadowRadius: 10,
        shadowColor: 'red'
    }

})
